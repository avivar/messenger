const dateOlderExample1 = new Date();
dateOlderExample1.setFullYear(2019)
dateOlderExample1.setHours(5)
const dateOlderExample2 = new Date();
dateOlderExample2.setFullYear(2016)
dateOlderExample2.setHours(8)

export default [
    {
        id: 1,
        recipientId: 2,
        authorId: 99,
        message: 'Message from me',
        timestamp: new Date().getTime()
    },
    {
        id: 2,
        recipientId: 1,
        authorId: 99,
        message: 'Message from me',
        timestamp: new Date().getTime()
    },
    {
        id: 3,
        recipientId: 1,
        authorId: 99,
        message: 'Message from me',
        timestamp: dateOlderExample1.getTime()
    },
    {
        id: 4,
        recipientId: 2,
        authorId: 99,
        message: 'Message from me',
        timestamp: dateOlderExample1.getTime()
    },
    {
        id: 5,
        recipientId: 3,
        authorId: 99,
        message: 'Message from me',
        timestamp: new Date().getTime()
    },
    {
        id: 6,
        recipientId: 99,
        authorId: 2,
        message: 'Message from Shlomo 2',
        timestamp: new Date().getTime()
    },
    {
        id: 7,
        recipientId: 99,
        authorId: 2,
        message: 'Message from Shlomo 2',
        timestamp: dateOlderExample2.getTime()
    },
    {
        id: 8,
        recipientId: 3,
        authorId: 99,
        message: 'Message from me',
        timestamp: new Date().getTime()
    },
    {
        id: 9,
        recipientId: 3,
        authorId: 1,
        message: 'Message from Shlomo 1',
        timestamp: dateOlderExample2.getTime()
    },
    {
        id: 10,
        recipientId: 3,
        authorId: 1,
        message: 'Message from Shlomo 1',
        timestamp: new Date().getTime()
    },
    {
        id: 11,
        recipientId: 2,
        authorId: 1,
        message: 'Message from Shlomo 1',
        timestamp: new Date().getTime()
    },
    {
        id: 12,
        recipientId: 3,
        authorId: 1,
        message: 'Message from Shlomo 1',
        timestamp: dateOlderExample2.getTime()
    },
    {
        id: 13,
        recipientId: 3,
        authorId: 1,
        message: 'Message from Shlomo 1',
        timestamp: new Date().getTime()
    },
    {
        id: 14,
        recipientId: 1,
        authorId: 3,
        message: 'Message from Shlomo 3',
        timestamp: new Date().getTime()
    },
    {
        id: 15,
        recipientId: 2,
        authorId: 1,
        message: 'Message from Shlomo 1',
        timestamp: new Date().getTime()
    },
    {
        id: 16,
        recipientId: 2,
        authorId: 1,
        message: 'Message from Shlomo 1',
        timestamp: new Date().getTime()
    },
    {
        id: 17,
        recipientId: 2,
        authorId: 3,
        message: 'Message from Shlomo 3',
        timestamp: new Date().getTime()
    },
    {
        id: 18,
        recipientId: 99,
        authorId: 3,
        message: 'Message from Shlomo 3',
        timestamp: new Date().getTime()
    },
    {
        id: 19,
        recipientId: 1,
        authorId: 2,
        message: 'Message from Shlomo 2',
        timestamp: new Date().getTime()
    },
    {
        id: 20,
        recipientId: 1,
        authorId: 2,
        message: 'Message from Shlomo 2',
        timestamp: new Date().getTime()
    },
]