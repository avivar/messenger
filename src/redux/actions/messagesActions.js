import { FETCH_MESSAGES, ADD_MESSAGE } from '../actionsConstants'

export const fetchMessages = () => {
    return ({ type: FETCH_MESSAGES });
}

export const addMessage = (message) => {
    return ({ type: ADD_MESSAGE, message });
}