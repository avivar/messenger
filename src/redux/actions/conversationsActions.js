import { SET_ACTIVE_CONVERSATION, SET_CONVERSATION_SEARCH_TERM, ADD_NEW_CONVERSATION } from '../actionsConstants'

export const setActiveConversation = (activeConversation) => {
    return ({ type: SET_ACTIVE_CONVERSATION, activeConversation });
}

export const setConversationSearchTerm = (conversationSearchTerm) => {
    return ({ type: SET_CONVERSATION_SEARCH_TERM, conversationSearchTerm });
}

export const addNewConversation = (conversationName) => {
    return ({ type: ADD_NEW_CONVERSATION, conversationName });
}