import { FETCH_MESSAGES, ADD_MESSAGE } from '../actionsConstants'
import staticMessages from '../../static/messagesObject';

const initState = {
    messages: staticMessages
}

const messagesReducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_MESSAGES:
            return state.messages;
        case ADD_MESSAGE:
            let message = action.message;
            return {
                ...state,
                messages: [...state.messages, message]
            };
        default:
            return state;
    }
}

export default messagesReducer