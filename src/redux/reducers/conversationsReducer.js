import { SET_ACTIVE_CONVERSATION, SET_CONVERSATION_SEARCH_TERM, ADD_NEW_CONVERSATION } from '../actionsConstants'
import staticConversations from '../../static/conversationsObject';

const initState = {
    conversations: staticConversations,
    activeConversation: {},
    conversationSearchTerm: ''
}

const conversationsReducer = (state = initState, action) => {
    switch (action.type) {
        case SET_ACTIVE_CONVERSATION:
            let activeConversation = action.activeConversation;
            return {
                ...state,
                activeConversation
            };
        case SET_CONVERSATION_SEARCH_TERM:
            let conversationSearchTerm = action.conversationSearchTerm;
            return {
                ...state,
                conversationSearchTerm
            };
        case ADD_NEW_CONVERSATION:
            let conversationName = action.conversationName;
            return {
                ...state,
                conversations: [
                    ...state.conversations,
                    {
                        authorId: 4,
                        photo: 'annonymous',
                        name: conversationName,
                        text: ''
                    }
                ]
            };
        default:
            return state;
    }
}

export default conversationsReducer
