import { combineReducers } from 'redux'
import messagesReducer from './messagesRedcuer';
import conversationsReducer from './conversationsReducer';

const rootReducer = combineReducers({
    messagesReducer,
    conversationsReducer
})

export default rootReducer