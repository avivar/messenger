import React from 'react';
import moment from 'moment';
import './message.css';

const Message = (props) => {
    const {
        data,
        isMyMessage,
        startSequence,
        endSequence,
        showTimestamp
    } = props;
    const messageClassName = `message${isMyMessage ? ' mine': ''}${startSequence ? ' start' : ''}${endSequence ? ' end' : ''}`;
    const timestamp = moment(data.timestamp).format('LLLL');
    return (
        <div className={messageClassName}>
            {
                showTimestamp && 
                <div className='timestamp'>
                    {timestamp}
                </div>
            }
        <div className='bubble-container'>
            <div className='bubble' title={timestamp}>
                {data.message}
            </div>
        </div>
        </div>
    )
}

export default Message;