import React, { useState } from 'react';
import './composeMessage.css'
import ActionButton from '../../components/ActionButton'
import { CameraTwoTone, FileImageTwoTone, AudioFilled, SmileTwoTone, SendOutlined } from '@ant-design/icons'
import { useDispatch, useSelector } from "react-redux";
import { addMessage } from '../../redux/actions/messagesActions'

const ComposeMessage = (props) => {
    const ENTER_KEY_CODE = 13;
    const [ messageContent, setMessageContent ] = useState('');
    const dispatch = useDispatch();
    const { authorId } = useSelector(state => state.conversationsReducer.activeConversation);
    const addNewMessage = () => {
        if(messageContent){
            let message = {
                id: 200,
                authorId: 99,
                message: messageContent,
                recipientId: authorId,
                timestamp: new Date().getTime()
            };
            setMessageContent('');
            dispatch(addMessage(message));
        }
    }
    const handleSendMessage = (event) => {
        if(event.keyCode === ENTER_KEY_CODE){
            addNewMessage();
        }
    }
    return (
        <div className='compose-message'>
            <input onKeyUp={handleSendMessage} value={messageContent} onChange={e => setMessageContent(e.target.value)} id='messageContent' type='text' className='text-input' placeholder='Type a message'/>
            <div className='action-buttons-compose'>
                <ActionButton action={addNewMessage} icon={<SendOutlined />} key='send'/>
                <ActionButton icon={<CameraTwoTone />} key='camera'/>
                <ActionButton icon={<FileImageTwoTone />} key='image'/>
                <ActionButton icon={<SmileTwoTone />} key='emoji'/>
                <ActionButton icon={<AudioFilled />} className={'filled'} key='record'/>
            </div>
        </div>
    )
}

export default ComposeMessage;