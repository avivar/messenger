import React, { useState } from 'react';
import './newConversation.css';
import { useDispatch } from "react-redux";
import { addNewConversation } from '../../redux/actions/conversationsActions'
import { Input, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';

const NewConversation = (props) => {
    const [ conversationName, setConversationName ] = useState('');
    const dispatch = useDispatch();
    const onAddConversationClicked = (e) => {
        e.preventDefault();
        if(conversationName){
            dispatch(addNewConversation(conversationName))
            setConversationName('');
        }
    }
    const onConversationNameChanged = (e) => {
        setConversationName(e.target.value);
    }
    return (
        <div className='new-conversation-section'>
            <Input value={conversationName} size="large" placeholder="New Conversation Name" prefix={<UserOutlined />} onChange={onConversationNameChanged}/>
            <Button type='primary' onClick={onAddConversationClicked}>Submit</Button>
        </div>
    )
}

export default NewConversation;