import React from 'react';
import './actionButton.css';

const ActionButton = (props) => {
    const { icon, className, action } = props;
    return (
        <div className={`action-button ${className || ''}`} onClick={action}>
            {icon}
        </div>
    )
}

export default ActionButton;