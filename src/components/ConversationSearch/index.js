import React, { useState } from 'react';
import './conversationSearch.css';
import { useDispatch } from "react-redux";
import { setConversationSearchTerm } from '../../redux/actions/conversationsActions'
import { CloseCircleFilled } from '@ant-design/icons'

const ConversationSearch = () => {
    const [ searchTerm, setSearchTerm ] = useState('');
    const dispatch = useDispatch();
    const onSearchTermChanged = (searchTerm) => {
        dispatch(setConversationSearchTerm(searchTerm));
        setSearchTerm(searchTerm);
    }
    const clearSearchTerm = () => {
        dispatch(setConversationSearchTerm(''));
        setSearchTerm('');
    }
    return (
        <div className='conversation-search'>
            <input
                type='search'
                className='conversation-search-input'
                placeholder='Search For Messages'
                onChange={(e) => { onSearchTermChanged(e.target.value) }}
                value={searchTerm}
            />
            { searchTerm && <CloseCircleFilled className='clear-search-term' onClick={clearSearchTerm} /> }
        </div>
    )
}

export default ConversationSearch;