import React from 'react';
import './conversation.css';
import { Avatar } from 'antd';
import { useDispatch } from "react-redux";
import { setActiveConversation } from '../../redux/actions/conversationsActions'
import { UserOutlined } from '@ant-design/icons'

const Conversation = (props) => {
    const dispatch = useDispatch();
    const { photo, name, text, authorId } = props.data;
    const { isActive } = props;
    const MAX_MESSAGE_SNIPPET = 30;
    const onSetConversation = () => {
        dispatch(setActiveConversation({ authorId, name }));
    }
    return (
        <div className={`single-conversation ${isActive ? 'active-conversation' : ''}`} onClick={onSetConversation}>
            {
                photo === 'annonymous' ?
                <Avatar className='conversation-img' icon={<UserOutlined/>} /> :
                <img className='conversation-img' src={photo} alt='conversation'/>
            }
            <div className='conversation-info'>
                <h1 className='conversation-title'>{name}</h1>
                <p className='conversation-snippet'>{text.length > MAX_MESSAGE_SNIPPET ? `${text.substring(0, MAX_MESSAGE_SNIPPET - 1)}...` : text}</p>
            </div>
        </div>
    )
}

export default Conversation;