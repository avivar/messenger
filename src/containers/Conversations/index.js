import React from 'react';
import Conversation from '../../components/Conversation';
import ConversationSearch from '../../components/ConversationSearch';
import Messagebar from '../../components/Messagebar';
import ActionButton from '../../components/ActionButton';
import './conversations.css';
import { SettingTwoTone, PlusCircleTwoTone } from '@ant-design/icons'
import { useSelector } from 'react-redux'
import { Popover, Button } from 'antd'
import NewConversation from '../../components/NewConversation'

const Conversations = (props) => {
    const { name: activeConversationName, authorId } = useSelector(state => state.conversationsReducer.activeConversation);
    const conversations = useSelector(state => state.conversationsReducer.conversations);
    const conversationSearchTerm = useSelector(state => state.conversationsReducer.conversationSearchTerm);
    
    return (
        <div className='conversation-list'>
            <Messagebar
                title='Messenger-Front-End'
                leftItems={[
                    <ActionButton icon={<SettingTwoTone />} key='settings'/>
                ]}
                rightItems={[
                    <Popover placement={'rightBottom'} content={<NewConversation/>} title="Add New Conversation" trigger="click" key='add-popover'>
                        <Button shape={'circle'} className='add-new-conversation-btn' ></Button>
                        <ActionButton icon={<PlusCircleTwoTone />} key='add'>
                        
                        </ActionButton>
                    </Popover>
                ]}
            />
            <ConversationSearch/>
            {
                conversations.map((conversation) => {
                    return (!conversationSearchTerm || (conversation.name.toLowerCase().includes(conversationSearchTerm.toLowerCase())) || conversation.text.toLowerCase().includes(conversationSearchTerm.toLowerCase()))
                    && <Conversation key={conversation.name} data={conversation} isActive={conversation.name === activeConversationName} />
                })
            }
        </div>
    )
}

export default Conversations;