import React, { useRef, useEffect } from 'react';
import moment from 'moment';
import Message from '../../components/Message';
import Messagebar from '../../components/Messagebar'
import ActionButton from '../../components/ActionButton'
import ComposeMessage from '../../components/ComposeMessage';
import { PhoneFilled, VideoCameraFilled, InfoCircleTwoTone, WechatOutlined } from '@ant-design/icons'
import { useSelector } from 'react-redux'
import './messages.css';

const MY_AUTHOR_ID = 99;
const HOURS = 'hours';

const Messages = () => {
    const messagesEndRef = useRef();
    useEffect(() => {
        scrollDown();
    });
    const scrollDown = () => {
        messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
    }
    const filterMessages = (message) => {
        return ((message.authorId === activeAuthorId && message.recipientId === MY_AUTHOR_ID) ||
        (message.authorId === MY_AUTHOR_ID && message.recipientId === activeAuthorId));
    }
    let messages = useSelector(state => state.messagesReducer.messages);
    const { name: activeConversationName, authorId: activeAuthorId } = useSelector(state => state.conversationsReducer.activeConversation);
    const renderMessages = () => {
        if(!activeAuthorId){
            return null;
        }
        messages = messages.filter(filterMessages);
        let allMessages = [];
        let sortedMessages = messages.sort((a,b) => (a.timstamp > b.timestamp) ? 1 : ((a.timestamp < b.timestamp) ? -1 : 0));
        for (let i = 0; i < sortedMessages.length; i++) {
            let previousMessage = i > 0 ? sortedMessages[i-1] : null;
            let currentMessage = sortedMessages[i];
            let nextMessage = (i < sortedMessages.length - 1) ? sortedMessages[i+1] : null;
            let isMyMessage = currentMessage.authorId === MY_AUTHOR_ID;
            let currentTimestamp = moment(currentMessage.timestamp);
            let prevBySameAuthor = false;
            let nextBySameAuthor = false;
            let startSequence = true;
            let endSequence = true;
            let showTimestamp = true;
            if(previousMessage){
                let previousTimestamp = moment(previousMessage.timestamp);
                let previousDuration = moment.duration(currentTimestamp.diff(previousTimestamp));
                prevBySameAuthor = previousMessage.author === currentMessage.author;
                if(prevBySameAuthor && previousDuration.as(HOURS) < 1){
                    startSequence = false;
                }
                if(previousDuration.as(HOURS) < 1){
                    showTimestamp = false;
                }
            }
            if(nextMessage && previousMessage){
                let nextTimestamp = moment(nextMessage.timestamp);
                let nextDuration = moment.duration(currentTimestamp.diff(nextTimestamp));
                nextBySameAuthor = previousMessage.author === currentMessage.author;
                if(nextBySameAuthor && nextDuration.as(HOURS) < 1){
                    endSequence = false;
                }
            }

            allMessages.push(
                <Message
                    key={i}
                    isMyMessage={isMyMessage}
                    startSequence={startSequence}
                    endSequence={endSequence}
                    showTimestamp={showTimestamp}
                    data={currentMessage}
                />
            )
        }
        return allMessages.length > 0 ? allMessages : null;
    }

    return (
        <div className='messages-list'>
            <Messagebar
                title={activeConversationName}
                rightItems={[
                    <ActionButton icon={<InfoCircleTwoTone className='toolbar-icon'/>} key='info'/>,
                    <ActionButton icon={<VideoCameraFilled className='toolbar-icon' />} key='video'/>,
                    <ActionButton icon={<PhoneFilled className='toolbar-icon' />} key='phone'/>,
                ]}
            />
            <div className='messages-list-container'>
                {renderMessages() || 
                    <div className='empty-messages'>
                        <div className='empty-messages-annotation'>
                            <p>Pick a conversation to start</p>
                        </div>
                        <WechatOutlined className='empty-messages-image'/>
                    </div>
                }
                <div ref={messagesEndRef} />
            </div>
            {activeAuthorId && <ComposeMessage/>}
        </div>
    )
}

export default Messages;