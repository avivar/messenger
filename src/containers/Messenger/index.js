import React, { Component } from "react"
import Messages from '../Messages'
import Conversations from '../Conversations'
import 'antd/dist/antd.css'
import './messenger.css';

const Messenger = () => {
  return (
    <div className='messenger'>
      <div className="scrollable sidebar">
          <Conversations />
        </div>
        <div id='messagesScroller' className="scrollable content">
          <Messages />
        </div>
    </div>
  )
}

export default Messenger;