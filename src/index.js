import React from 'react'
import ReactDOM from 'react-dom'
import Messenger from './containers/Messenger';
import { HashRouter } from 'react-router-dom'
import store from './redux/store'
import { Provider } from 'react-redux'

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <Messenger/>
        </HashRouter>
    </Provider>
, document.getElementById('root'));