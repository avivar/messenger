const webpack = require('webpack');
const format = require('react-dev-utils/formatWebpackMessages');
const { join } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { babelRule, fileLoaderRule, miniCssRule, MiniCssExtractPlugin, appName } = require('./config.common');
const PRODUCTION = 'production';
const { EOL } = require('os');
const chalk = require('chalk');

const environmentVariablePlugin = new webpack.DefinePlugin({
    'ENVIRONMENT': JSON.stringify(PRODUCTION)
})

const htmlPlugin = new HtmlWebpackPlugin({
    template: join(process.cwd(), 'src', 'index.html'),
    filename: 'index.html'
})

const miniCssExtractPlugin = new MiniCssExtractPlugin({
    filename: 'css/[name].[hash].css',
    chunkFilename: 'css/[id].[hash].css',
});

const configProd = {
    name: appName,
    mode: PRODUCTION,
    entry: join(process.cwd(), 'src', 'index.js'),
    // externals: {
    //     'react': 'React',
    //     'react-dom': 'ReactDOM'
    // },
    output: {
        path: join(process.cwd(), 'build'),
        filename: 'js/bundle.prod.js'
    },
    module: {
        strictExportPresence: true,
        rules: [ babelRule, miniCssRule, fileLoaderRule ]
    },
    plugins: [
        environmentVariablePlugin,
        new webpack.NamedModulesPlugin,
        htmlPlugin,
        miniCssExtractPlugin,
    ],
    optimization: {
        minimize: true
    }
}

const writeLogs = (errors, warnings) => {
    let buildMessage = '';
    if(errors && errors.length){
        console.log(chalk.bgRed('***ERRORS***'));
        errors.forEach(err => console.log(err));
        buildMessage = chalk.redBright('Build failed!');
    }
    else if(warnings && warnings.length){
        console.log(chalk.black.bgYellowBright('***WARNINGS***'));
        warnings.forEach(warn => console.log(warn));
        buildMessage = chalk.yellowBright('Build succeeded with warninngs!');
    }
    else{
        buildMessage = chalk.greenBright('Build succeeded!');
    }
    console.log(`${EOL}${buildMessage}`);
}

const compiler = webpack(configProd);
compiler.run((err, stats) => {
    if(err){
        throw err;
    }
    const { errors, warnings } = format(stats.toJson({}, true));
    writeLogs(errors, warnings);
})