module.exports = {
    hot               : true,
    quiet             : true,
    overlay           : true,
    compress          : true,
    disableHostCheck  : true,
    watchContentBase  : true,
    historyApiFallback: true,
    watchOptions      : { ignored: /node_modules/ },
}